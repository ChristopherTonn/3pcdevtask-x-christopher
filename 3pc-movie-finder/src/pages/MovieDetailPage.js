import React, { Component } from "react";
import { Link } from "react-router-dom";
import '../index.css'

class MovieDetailPage extends Component {

    movie = {};

    constructor() {
        super();
        this.state = {
          isLoading: false
        };
      }

    componentDidMount() {
        this.fetchMovieDetails();
    }

    fetchMovieDetails = async () => {

        const apiBaseUrl = process.env.REACT_APP_MOVIE_DB_API;
        const apiKey = process.env.REACT_APP_API_KEY;
        const movieId = this.props?.match.params.movieId;
        const searchUrl = apiBaseUrl + '/movie/' + movieId + '?api_key=' + apiKey + '&language=en-US';
       
        this.setState({isLoading: true})
    
        try {
          console.log('Url: ', searchUrl);
    
          const response = await fetch(searchUrl);
          const data = await response.json();

          this.movie = data;
    
          console.log('Movie: ', this.movie);
          
        } catch (error) {
          console.error(error);
        }
    
        this.setState({isLoading: false});
      };

   
    render(){
        return(

            <main className="container-fluid">
                <article className="row">
                    <section className="row">
                        <Link to="/home"><button className="offset-md-1 col-md-1  basic-btn back-btn">Back</button></Link>
                    </section>
                    <section className="detail-page__wrapper row d-flex justify-content-center">
                        <div className="col-md-3">
                            <object className="detail-page__img img-fluid" data={process.env.REACT_APP_IMAGE_BASE_PATH + '/w500' + this.movie.poster_path} type="image/png">
                                    <img className="img-fluid" src="https://images.unsplash.com/photo-1560109947-543149eceb16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2775&q=80" alt="Default Image" loading="lazy"/>
                            </object>    
                        </div>
                        <div className="col-md-5 detail-page__info-wrapper">
                            <h2 className="detail-page__titel">{this.movie?.original_title}</h2>
                            <h3 className="detail-page__sub-headline">Rating</h3>
                            <p className="detail-page__sub-info-text">{this.movie?.vote_average} / 10</p>
                            <p className="detail-page__overview-text">{this.movie?.overview}</p>
                            <h3 className="detail-page__sub-headline">Release</h3>
                            <p className="detail-page__sub-info-text">{this.movie?.release_date}</p>
                        </div>
                    </section>
                    
                    {this.state.isLoading && <p>LOADING...</p>}
                    {!this.state.isLoading && this.movie === undefined && <p>Found No Movie...</p>}
                </article>
            </main>
            
          

            
        );
    }
}

export default MovieDetailPage;