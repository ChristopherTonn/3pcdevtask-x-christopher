import React, { Component } from "react";
import { Link } from "react-router-dom";

class PreviewCard extends Component {


   
    render(){
        return(
            <Link to={"/movie/detail/" + this.props?.id}>
                <section className="col-md-2 preview-card__wrapper">
                    <div className="row">
                        <div className="d-flex justify-content-center">
                            <object className="img-fluid" data={process.env.REACT_APP_IMAGE_BASE_PATH + '/w500' + this.props?.poster} type="image/png">
                                    <img className="img-fluid preview-card__img" src="https://images.unsplash.com/photo-1560109947-543149eceb16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2775&q=80" alt="Default Image" loading="lazy"/>
                            </object>    
                        </div>
                    </div>
                    <div className="row">
                        <div className="d-flex justify-content-center">
                            <p className="preview-card__text">
                                {this.props?.titel}
                            </p>
                        </div>  
                    </div>
                </section>
            </Link>
           
            
        );
    }
}

export default PreviewCard;