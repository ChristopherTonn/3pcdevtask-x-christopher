import React, { Component, useState } from "react";
import { Route, Router } from "react-router-dom";
import MovieDetailPage from "./pages/MovieDetailPage";
import PreviewCard from "./shared/PreviewCard";




class App extends Component {

  constructor() {
    super();
    this.state = {
      movies: [],
      isLoading: false
    };
  }

  componentDidMount() {
    this.fetchTrendingMovies();
  }

  fetchTrendingMovies = async () => {

    const apiBaseUrl = process.env.REACT_APP_MOVIE_DB_API;
    const apiKey = process.env.REACT_APP_API_KEY;
    const searchUrl = apiBaseUrl + '/trending/all/day?api_key=' + apiKey;
   
    this.setState({isLoading: true})

    try {
      const response = await fetch(searchUrl);
      const data = await response.json();

      const sortedByPopularity = data.results?.sort( (a,b) => { return b.popularity - a.popularity} );
      this.state.movies = sortedByPopularity;

    } catch (error) {
      console.error(error);
    }

    this.setState({isLoading: false});
  };

  searchMovies = async (event) => {

    const apiBaseUrl = process.env.REACT_APP_MOVIE_DB_API;
    const apiKey = process.env.REACT_APP_API_KEY;
    const query = event.target.value;
    const searchUrl = apiBaseUrl + '/search/movie?api_key=' + apiKey + '&language=en-US&query=' + query + '&page=1&include_adult=false';
   
    event.preventDefault();
    this.setState({isLoading: true})

    try {
      const response = await fetch(searchUrl);
      const data = await response.json();

      const sortedByPopularity = data.results?.sort( (a,b) => { return b.popularity - a.popularity} );
      this.state.movies = sortedByPopularity;
      
    } catch (error) {
      console.error(error);
    }

    this.setState({isLoading: false});
  };

  render(){
    return (
      <>
      
        <Route path=":subpath(/home|/)">
                <main className="App">
                  <header className="d-flex justify-content-center">
                  <h1 className="headline">3pc movie finder</h1>
                  </header>
                  <article>
                    <section className="container-fluid">
                      <div className="row">
                        <form className="search-bar col-md-12 d-flex justify-content-center">
                          <input className="search-bar__input col-md-6"  onKeyUp={this.searchMovies} type="text" name="query" placeholder="Name the movie you looking for."/>
                          <button className="search-bar__button basic-btn col-md-1" onClick={this.searchMovies} type="submit">SEARCH</button>
                        </form>
                      </div>
                    </section>
                    <div></div>
                    <section className="container">
                      <div className="row">
                        {this.state.isLoading && <p>LOADING...</p>}
                        {!this.state.isLoading && this.state.movies?.length === 0 && <p>Found No Movies...</p>}
                        {!this.state.isLoading &&  this.state.movies?.length > 0 &&
                                                  <div>
                                                    {this.state.movies?.map(item => {
                                                        return  <PreviewCard id={item?.id} titel={item?.original_title} poster={item.poster_path}></PreviewCard>;
                                                    })}
                                                  </div>}
                      </div>
                    </section>
                  </article>
                </main>
          </Route>
            
          <Route path="/movie/detail/:movieId" component={MovieDetailPage}>
          </Route>
      
      </>
    );
  }
  
}

export default App;
